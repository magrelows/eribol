//
//  GastosModel.swift
//  ERIBOL
//
//  Created by entelgy on 06/05/19.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import Foundation
import CoreData

public class GastosModel {
    var gastos: NSManagedObject?
    var id: UUID?
    var data: Date?
    var detalhe: String?
    var valor_gasto: Decimal?
    
    func salvarGasto(gasto: GastosModel) -> GastosModel {
        return GastosRepository().salvarRepositorio(gastos: gasto)!
    }
    
    func retornaTodosGastos() -> [GastosModel] {
        return GastosRepository().retornarTodas()!
    }
}
