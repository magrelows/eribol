//
//  GastosRepository.swift
//  ERIBOL
//
//  Created by entelgy on 06/05/19.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class GastosRepository {
    let key = "Gastos"
    
    public func salvarRepositorio( gastos: GastosModel ) -> GastosModel? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: key, in: context)
        let novoGasto = NSManagedObject(entity: entity!, insertInto: context)
        
        novoGasto.setValue(gastos.id, forKey: "id")
        novoGasto.setValue(gastos.data, forKey: "data")
        novoGasto.setValue(gastos.detalhe, forKey: "detalhe")
        novoGasto.setValue(gastos.valor_gasto, forKey: "valor_gasto")
        
        do {
            try context.save()
            gastos.gastos = novoGasto
            print("salvou com sucesso a tarefa")
            return gastos
        } catch {
            print("falha ao salvar tarefa")
            return nil
        }
    }
    
    public func retornarTodas () -> [GastosModel]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: key)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            let todosGastos = converteNSObjectParaGastos(objects: result as! [NSManagedObject])
            return todosGastos
        } catch {
            print("Failed")
        }
        
        return nil
    }
    
    private func converteNSObjectParaGastos( objects : [NSManagedObject]) -> [GastosModel] {
        var gastosLista: [GastosModel] = []
        
        for data in objects {
            let gastosAux = GastosModel()
            gastosAux.id = data.value(forKey: "id") as? UUID
            gastosAux.data = data.value(forKey: "data") as? Date
            gastosAux.detalhe = data.value(forKey: "detalhe") as? String
            gastosAux.valor_gasto = data.value(forKey: "valor_gasto") as? Decimal
            gastosAux.gastos = data
            
            gastosLista.append(gastosAux)
        }
        
        return gastosLista
    }
}
