//
//  ListTableViewCell.swift
//  ERIBOL
//
//  Created by entelgy on 06/05/19.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    let monthLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Mounth"
        
        return label
    }()
    
    let valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Values"
        
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createMonthLabel()
        self.createValueLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //Creating AUTOLAYOUT Programatically
    func createMonthLabel () {
        self.addSubview(monthLabel)
        monthLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        monthLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
    }
    
    func createValueLabel () {
        self.addSubview(valueLabel)
        
        valueLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
    }
    
}
