//
//  ListView.swift
//  ERIBOL
//
//  Created by entelgy on 06/05/19.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit

private let reusableCell = "listTableViewCell"

class ListView: UIView {
    let confirmButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
    let listTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()

    
    //Creating Autolayout programatically
    func setupInit() {
        self.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.createConfirmButton()
        self.createTableView()
        
        listTableView.delegate = self
        listTableView.dataSource = self
    }
    
    func createConfirmButton () {
        self.addSubview(confirmButton)
        confirmButton.leadingAnchor.constraint(equalToSystemSpacingAfter: self.safeAreaLayoutGuide.leadingAnchor, multiplier: 0).isActive = true
        confirmButton.trailingAnchor.constraint(equalToSystemSpacingAfter: self.safeAreaLayoutGuide.trailingAnchor, multiplier: 0).isActive = true
        confirmButton.bottomAnchor.constraint(equalToSystemSpacingBelow: self.bottomAnchor, multiplier: 0).isActive = true
        confirmButton.heightAnchor.constraint(equalToConstant: 114).isActive = true
        
        confirmButton.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        confirmButton.setTitle("Adicionar", for: .normal)
    }
    
    func createTableView() {
        self.addSubview(listTableView)
        listTableView.leadingAnchor.constraint(equalToSystemSpacingAfter: self.safeAreaLayoutGuide.leadingAnchor, multiplier: 0).isActive = true
        listTableView.trailingAnchor.constraint(equalToSystemSpacingAfter: self.safeAreaLayoutGuide.trailingAnchor, multiplier: 0).isActive = true
        listTableView.topAnchor.constraint(equalToSystemSpacingBelow: self.topAnchor, multiplier: 0).isActive = true
        listTableView.bottomAnchor.constraint(equalToSystemSpacingBelow: self.confirmButton.topAnchor, multiplier: 0).isActive = true
        listTableView.rowHeight = 60
        self.registerNibs()
    }
    
    private func registerNibs() {
        listTableView.register(ListTableViewCell.self, forCellReuseIdentifier: reusableCell)
    }
}

//VIEW DELEGATES
extension ListView: UITableViewDelegate, UITableViewDataSource {
    //Table View delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableCell, for: indexPath) as! ListTableViewCell
        //cell.textLabel?.text = "teste"
        return cell
    }
}
