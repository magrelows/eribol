//
//  ListViewController.swift
//  ERIBOL
//
//  Created by entelgy on 06/05/19.
//  Copyright © 2019 ERIMIA. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
   
    fileprivate var mainView: ListView {
        let viewInit = ListView()
        viewInit.frame = view.bounds
        viewInit.setupInit()
        return viewInit
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.controllerInit()
        //self.dummyGasto()
    }
    
    func controllerInit() {
        self.view.addSubview(mainView)
    }
    
    /*func dummyGasto() {
        var gastoDummy = GastosModel()
        gastoDummy.id = UUID()
        gastoDummy.detalhe = "UM GASTO QUE FOI REALIZADO"
        gastoDummy.data = Date()
        gastoDummy.valor_gasto = 20.00
        
        gastoDummy = GastosModel().salvarGasto(gasto: gastoDummy)
        
        let gastosGeral = GastosModel().retornaTodosGastos()
        print(gastosGeral)
    }*/
}
